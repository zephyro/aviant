<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
        <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
        <link rel="stylesheet" href="css/main.css">

    </head>
    <body>

        <div class="page">

            <header class="header">
                <div class="container">
                    <div class="header__top">
                        <div class="header__logo">
                            <img src="img/logo.svg" class="img-fluid" alt="">
                        </div>
                        <ul class="header__contact">
                            <li>
                                <a class="header__phone" href="tel:+74999990132">+ 7(499)999-01-32</a>
                            </li>
                            <li>
                                <a href="#" class="btn text-uppercase">Бесплатный звонок</a>
                            </li>
                        </ul>
                    </div>
                    <div class="header__content">
                        <h1>«Авиант Прайм»-построение алгоритмов для <span class="color_yellow">реального</span> бизнеса</h1>
                        <div class="header__slogan">Запрограммируйте свой бизнес на успех!</div>
                        <ul class="header__advantage">
                            <li><span class="color_yellow">20 лет</span> построения алгоритмов бизнеса</li>
                            <li><span class="color_yellow">1000+</span> успешных проектов</li>
                            <li>Индивидуальный, комплексный подход</li>
                        </ul>
                    </div>
                </div>
            </header>

            <section class="main">
                <div class="container">

                    <div class="tile tile_invert">
                        <div class="tile__image">
                            <div class="tile__image_wrap">
                                <img src="img/tile_01.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="tile__text">
                            <h4>Ваш бизнес в тупике?</h4>
                            <p>
                                Работа похожа на беличье колесо: движение есть, а результата никакого?
                                В лучшем случае колесо буксует на месте, в худшем – катится вниз. Вы
                                нанимаете очередного топ-менеджера для спасения компании, но…
                                результата как не было, так и нет? И это логично. Топ-менеджер не строит
                                систем – он их обслуживает. А Вам нужны именно системные изменения.
                                К сожалению, ситуация имеет тенденцию развиваться от плохого к худшему.
                                И если не принять меры прямо сейчас, позже может быть уже слишком
                                поздно. Холодные цифры статистики подтверждают это:
                            </p>
                            <a href="#">Посмотреть статистику банкротств в РФ</a>
                        </div>
                    </div>

                    <div class="tile">
                        <div class="tile__image">
                            <div class="tile__image_wrap">
                                <img src="img/tile_02.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="tile__text">
                            <h4>Ваш бизнес уперся в «потолок»?</h4>
                            <p>В Вашем бизнесе все хорошо, но любые попытки перейти на следующую
                                ступень развития заканчиваются ничем. Как будто над Вами стеклянный
                                потолок, преодолеть который не представляется возможным. Знакомо?</p>
                            <p>Можно смириться и жить дальше – вроде бы ситуация в целом неплохая.
                                Неплохая, но рискованная. Внешние условия меняются, потолок будет
                                опускаться, пока не раздавит всех, кто не смог его пробить. Остановка в
                                бизнесе –медленная смерть.</p>
                        </div>
                    </div>

                    <div class="tile tile_invert">
                        <div class="tile__image">
                            <div class="tile__image_wrap">
                                <img src="img/tile_03.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="tile__text">
                            <h4>Хотите от бизнеса большего?</h4>
                            <p>Ваш бизнес устойчив, системный подход и автоматизация бизнес-
                                процессов позволяет Вам держать руку на пульсе. Вы знаете, что такое
                                опережающие показатели и управляете компанией с их помощью.
                                Ваша команда слажена, а текучка кадров отсутствует. </p>
                            <p>Следующий шаг – освоение нового. Новые территории, новые
                                направления, новые сегменты рынка. Сложность, которая Вас ждет –
                                старая система не сможет работать в новых условиях. Ваша отлаженная
                                бизнес-машина забуксует на непаханой новой земле.</p>
                        </div>
                    </div>

                    <div class="heading mb_40">
                        <h2>Пришло время действовать</h2>
                        <div class="heading__subtitle">5 шагов к системному бизнесу с помощью алгоритмов  «Авиант Прайм»</div>
                    </div>

                    <ul class="step mb_40">
                        <li>
                            <div class="step__number">1</div>
                            <div class="step__content">
                                <h4>Аудит текущего состояния бизнеса</h4>
                                <p>Описание бизнес-процессов «как есть». Делаем описание без прикрас, со всеми достоинствами и недостатками бизнеса. На этом этапе главное для собственника – честность, прежде всего с самим собой.</p>
                            </div>
                        </li>
                        <li>
                            <div class="step__number">2</div>
                            <div class="step__content">
                                <h4>Выявление слабых мест и точек роста</h4>
                                <p>На основе аудита находим поле проблем или выявляем точки роста бизнеса. Чаще всего, точки роста возникают в неожиданных для собственника местах. Это логично:замыленный взгляд не видит очевидных для стороннего эксперта вещей.</p>
                            </div>
                        </li>
                        <li>
                            <div class="step__number">3</div>
                            <div class="step__content">
                                <h4>Построение бизнес-процессов</h4>
                                <p>Написание  бизнес-процессов на бумаге «как должно быть». Разработка организационной структуры компании. Разработка KPI, должностны инструкций и обозначение зонответственности для топ-менеджеров.</p>
                            </div>
                        </li>
                        <li>
                            <div class="step__number">4</div>
                            <div class="step__content">
                                <h4>Создание ресурсной карты</h4>
                                <p>Выявление потребности в человеческом ресурсе (HR), составление ролевых моделей, аватаров должностей. Подбор и наем персонала, создание системы мотивации. Разработка систем планирования, прогнозирования и отчетности.</p>
                            </div>
                        </li>
                        <li>
                            <div class="step__number">5</div>
                            <div class="step__content">
                                <h4>Запуск системы</h4>
                                <p>Внедрение разработанной системы бизнеса. Подготовка компании к изменениям, тестирование новых бизнес-процессов , тонкая настройка алгоритмов бизнеса под реальные условия.</p>
                            </div>
                        </li>
                    </ul>

                    <div class="heading mb_30">
                        <h2>Результаты внедрения автоматизации</h2>
                    </div>

                    <div class="result mb_60">
                        <div class="result__slider swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="result__slider_item">
                                        <img src="images/slide.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="result__slider_item">
                                        <img src="images/slide.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="result__slider_item">
                                        <img src="images/slide.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="result__slider_item">
                                        <img src="images/slide.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                            <!-- Add Arrows -->
                            <div class="slider_button_next">
                                <img src="img/slider_arrow.svg" class="img-fluid" alt="">
                            </div>
                            <div class="slider_button_prev">
                                <img src="img/slider_arrow.svg" class="img-fluid" alt="">
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>


                    <div class="heading mb_40">
                        <h2>Эффект от внедрения системы алгоритмов бизнеса «Авиант Прайм»<br/>4 основных направления:</h2>
                    </div>

                    <div class="direction mb_40">

                        <div class="direction__item">
                            <div class="direction__image">
                                <img src="img/direction__01.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="direction__content">
                                <div class="direction__title"><span>Маркетинг</span></div>
                                <div class="direction__text">
                                    Привлечение клиентов<br/>
                                    Удержание клиентов<br/>
                                    Развитие маркетинговых каналов
                                </div>
                            </div>
                        </div>

                        <div class="direction__item">
                            <div class="direction__image">
                                <img src="img/direction__02.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="direction__content">
                                <div class="direction__title"><span>Продажи</span></div>
                                <div class="direction__text">
                                    Максимально возможная эффективность конвертации потенциальных клиентов в реальные.<br/>
                                    Быстрое введение в бой новичков<br/>
                                    Единые стандарты и скрипты продаж

                                </div>
                            </div>
                        </div>

                        <div class="direction__item">
                            <div class="direction__image">
                                <img src="img/direction__03.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="direction__content">
                                <div class="direction__title"><span>Финансы</span></div>
                                <div class="direction__text">
                                    Упорядочивание финансовых отношений<br/>
                                    Отсутствие кассовых разрывов<br/>
                                    Управление дебиторской задолженностью

                                </div>
                            </div>
                        </div>

                        <div class="direction__item">
                            <div class="direction__image">
                                <img src="img/direction__04.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="direction__content">
                                <div class="direction__title"><span>Производство</span></div>
                                <div class="direction__text">
                                    Сокращение издержек<br/>
                                    Повышение качества<br/>
                                    Снижение количества рекламаций
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="heading mb_30">
                        <h2>
                            Получить кейсы с результатами<br/>
                            из Вашей отрасли
                        </h2>
                    </div>

                    <form class="form">
                        <div class="form_inline">
                            <div class="form_inline__item">
                                <input class="form_control" name="email" placeholder="email">
                            </div>
                            <div class="form_inline__item">
                                <button type="submit" class="btn btn_send">Получить кейсы</button>
                            </div>
                        </div>
                    </form>

                </div>
            </section>

            <section class="special">
                <div class="container">

                    <div class="special__heading">
                        <h2 class="text-center mb_30">Специальное предложение</h2>
                        <div class="special__heading_text">Внедрите автоматизацию в Ваш бизнес  до 30 мая 2019г.</div>
                        <div class="special__heading_subtitle">и получите скидку 50%</div>
                    </div>

                    <div class="special__content">

                        <div class="special__wrap">

                            <div class="special__box">
                                <div class="special__info">
                                    <h4>Выезд эксперта по автоматизации</h4>
                                    <p>Сделайте первый шаг к росту вашего бизнеса!</p>
                                    <p>Закажите полноценный выезд эксперта автоматизации к вам в офис.</p>
                                    <p>
                                        Доступным языком, без тумана он расскажет как автоматизация может
                                        повлиять на точки роста вашего бизнеса. Профессиональный взгляд,
                                        мини-диагностика и проф. помощь в выборе стратегии автоматизации.
                                        Любые ваши вопросы будут подробнейшим образом прокомментированы
                                        и систематизированы компетентным специалистом. Встреча с
                                        экспертом позволит создать у вас как с помощью автоматизации закрыть.
                                    </p>
                                </div>
                                <div class="special__result">
                                    <h4>Что на выходе:</h4>
                                    <ul>
                                        <li>• Чек-лист действий по автоматизации бизнеса</li>
                                        <li>• Оценочна диагностика текущего состояния автоматизации с рекомендациями</li>
                                        <li>• Эксперты уровня директор работают , прямой доступ</li>
                                    </ul>
                                </div>
                                <div class="special__summary">
                                    <div class="special__summary_price">
                                        <span>16000</span>
                                        <strong>8000 руб.</strong>
                                    </div>
                                    <div class="special__summary_button">
                                        <a class="btn" href="#">Оплатить онлайн</a>
                                    </div>
                                </div>
                            </div>


                            <div class="special__box">
                                <div class="special__info">
                                    <h4>Аудит  и тех.надзор проекта автоматизации</h4>
                                    <p>
                                        Вы уже находитесь в проекте по автоматизации, но пока проект не
                                        приносит вам ничего кроме проблем и бессмысленно потраченного
                                        времени?  Вы уверены, что проектная команда действительно
                                        профессионалы и им можно доверить такой дорогостоящий и
                                        стратегически важный проект для бизнеса?
                                    </p>
                                    <p>Предлагаем подключить наших экспертов и получить независимый взгляд на текущую автоматизацию.</p>
                                    <p>Наши эксперты будут оценивать работу подрядчиков, принимать результаты и нести полную ответственность за проект автоматизации.</p>

                                </div>
                                <div class="special__result">
                                    <h4>Что на выходе:</h4>
                                    <ul>
                                        <li>• Разработка  Стратегии автоматизации</li>
                                        <li>• Текущая оценка автоматизации бизнеса</li>
                                        <li>• Разработка рекомендаций по автоматизации бизнеса</li>
                                        <li>• Регулярный аудит процесса автоматизации</li>
                                    </ul>
                                </div>
                                <div class="special__summary">
                                    <div class="special__summary_price">
                                        <span>80000</span>
                                        <strong>50000 руб.</strong>
                                    </div>
                                    <div class="special__summary_button">
                                        <a class="btn" href="#">Оплатить онлайн</a>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
            </section>

            <div class="heading_white">
                <div class="container">
                    <h2>Доверьте развитие бизнеса «Авиант Прайм»</h2>
                </div>
            </div>

            <section class="expert">
                <div class="container">
                    <div class="expert__row">
                        <div class="expert__user">
                            <div class="expert__user_wrap">
                                <div class="expert__user_title">Ведущие эксперты</div>

                                <div class="expert__user_item active" id="user_01">
                                    <div class="expert__user_photo">
                                        <img src="images/user__photo.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="expert__user_name">Елена Сгибнева</div>
                                    <div class="expert__user_text">
                                        Директор Департамента Внедрения,
                                        сертифицированный эксперт по продукту 1С ERP,
                                        1С Управление холдингом, Sap ERP, ведущий куратор
                                        группы компаний Авиант, руководитель проектов
                                        автоматизации предприятий Роникон, Инфинити,
                                        Красная Заря и других.
                                    </div>
                                </div>

                                <div class="expert__user_item" id="user_02">
                                    <div class="expert__user_photo">
                                        <img src="images/user__photo.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="expert__user_name">Елена Ситнова</div>
                                    <div class="expert__user_text">
                                        Директор Департамента Внедрения,
                                        сертифицированный эксперт по продукту 1С ERP,
                                        1С Управление холдингом, Sap ERP, ведущий куратор
                                        группы компаний Авиант, руководитель проектов
                                        автоматизации предприятий Роникон, Инфинити,
                                        Красная Заря и других.
                                    </div>
                                </div>

                                <div class="expert__user_item" id="user_03">
                                    <div class="expert__user_photo">
                                        <img src="images/user__photo.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="expert__user_name">Алина Смирнова</div>
                                    <div class="expert__user_text">
                                        Директор Департамента Внедрения,
                                        сертифицированный эксперт по продукту 1С ERP,
                                        1С Управление холдингом, Sap ERP, ведущий куратор
                                        группы компаний Авиант, руководитель проектов
                                        автоматизации предприятий Роникон, Инфинити,
                                        Красная Заря и других.
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="expert__content">
                            <ul>
                                <li>• Мы улучшили бизнес сотен компаний. В нашем портфеле кейсы из самых разных сфер бизнеса. Мы не теоретики, оторванные от реальной жизни: наша система проверена 20 годами работы.</li>
                                <li>• Группа компаний «Авиант» решает задачи в комплексе: выстраивает бизнес-системы и на их основе выводит бизнес на новый уровень.</li>
                                <li>• Ведущий эксперт «Авиант Прайм» Сергей Мироненко - резидент и модератор закрытого бизнес - клуба «Эквиум Х».</li>
                                <li>• Компания федерального уровня – в России открыто 9 региональных офисов.</li>
                            </ul>
                            <div class="expert__nav">
                                <a href="#user_01" class="active">
                                    <i>
                                        <img src="images/user__photo.png" class="img-fluid" alt="">
                                    </i>
                                    <span>Елена Сгибнева</span>
                                </a>
                                <a href="#user_02">
                                    <i>
                                        <img src="images/user__photo.png" class="img-fluid" alt="">
                                    </i>
                                    <span>Елена Сгибнева</span>
                                </a>
                                <a href="#user_03">
                                    <i>
                                        <img src="images/user__photo.png" class="img-fluid" alt="">
                                    </i>
                                    <span>Елена Сгибнева</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="meeting">
                <div class="container">
                    <div class="meeting__wrap">
                        <div class="meeting__content">
                            <div class="meeting__content_title">Мир меняется ежедневно</div>
                            <div class="meeting__content_text">
                                <p>
                                    Мир изменился. Бизнес изменился. В конкурентной гонке теперь побеждает не самый сильный,
                                    а самый быстрый. Тот, кто сможет быстрее других адаптироваться к изменившимся условиям
                                    игры. Система алгоритмов  дает возможность опередить конкурентов
                                    в принятии правильных, и главное, своевременных решений.
                                </p>
                                <p>Сделайте первый шаг – обсудите с экспертом выгоды системы «Авиант Прайм» для Вашего бизнеса.</p>
                                <br/>
                                <p>Назначить встречу  нашему эксперту</p>
                            </div>
                            <div class="meeting__form">
                                <form class="form">
                                    <div class="form_inline">
                                        <div class="form_inline__item">
                                            <input class="form_control" name="email" placeholder="email">
                                        </div>
                                        <div class="form_inline__item">
                                            <button type="submit" class="btn btn_send">Получить кейсы</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="meeting__image">
                            <img src="img/meeting__image.jpg" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
            </section>

            <section class="main">
                <div class="container">
                    <div class="heading mb_45">
                        <h2>Варианты автоматизации вашего бизнеса</h2>
                    </div>

                    <div class="variant variant_invert">
                        <div class="variant__image">
                           <div class="embed-responsive embed-responsive-16by9">
                               <iframe src="https://www.youtube.com/embed/HZ6tWIUC5WY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                           </div>
                        </div>
                        <div class="variant__text">
                            <h4>Платформенная автоматизация</h4>
                            <p>
                                Это готовые алгоритмы, которые решают «общие стандартные» задачи:
                                начисление зарплат, приход-расход и т.д. Решения обкатаны годами
                                и подойдут всем организациям — от булочной до военного завода.
                            </p>
                        </div>
                    </div>

                    <div class="variant">
                        <div class="variant__image">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe src="https://www.youtube.com/embed/HZ6tWIUC5WY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="variant__text">
                            <h4>Реальная автоматизация</h4>
                            <p>
                                Алгоритмы, которые создаются под ваши внутренние стандартные задачи.
                                Например, расчет себестоимости товара или оценка затрат на логистику,
                                стрижку газонов и т.д.
                            </p>
                        </div>
                    </div>

                    <div class="variant variant_invert">
                        <div class="variant__image">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe src="https://www.youtube.com/embed/HZ6tWIUC5WY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="variant__text">
                            <h4>Интеллектуальная автоматизация</h4>
                            <p>
                                Это статистика в реальном времени: программа следит за верхними и
                                нижними границами показателей (лидогенерации, количества звонков и
                                т.п.). Если показатели упали или поднялись, вы узнаете об этом сразу, а не
                                в ежемесячном отчете.
                            </p>
                        </div>
                    </div>

                    <div class="variant">
                        <div class="variant__image">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe src="https://www.youtube.com/embed/HZ6tWIUC5WY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="variant__text">
                            <h4>Функциональная автоматизация</h4>
                            <p>
                                Решает ваши уникальные задачи. Продавцу в магазине нужно сообщение
                                о поступлении товара? Менеджеру нужно знать температуру воздуха на
                                складе, чтобы не продать клиенту испорченную краску? Функциональная
                                автоматизация поможет.
                            </p>
                        </div>
                    </div>

                    <div class="variant variant_invert mb_50">
                        <div class="variant__image">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe src="https://www.youtube.com/embed/HZ6tWIUC5WY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="variant__text">
                            <h4>Автоматизация Big Data</h4>
                            <p>
                                Строит гипотезы на основе данных о действиях клиента. Например:
                                во вторник с 9 до 11 в магазине много людей, а покупок мало, вывод –
                                настроить рекламу под этих людей или изменить раскладку на полке.
                                Это полноценная система планирования, ваша правая рука .
                            </p>
                        </div>
                    </div>

                    <div class="heading mb_40">
                        <h2>4 простых шага к новому уровню развития</h2>
                    </div>

                    <ul class="stage mb_40">
                        <li>
                            <div class="stage__icon">
                                <i>
                                    <img src="img/stage__01.svg" class="img-fluid" alt="">
                                </i>
                            </div>
                            <div class="stage__text">Звоните нам по тел.: <a href="tel:+74999990132">+ 7(499) 999-01-32</a> или <a href="#" class="btn_text">оставляете заявку</a></div>
                        </li>
                        <li>
                            <div class="stage__icon">
                                <i>
                                    <img src="img/stage__02.svg" class="img-fluid" alt="">
                                </i>
                            </div>
                            <div class="stage__text">Назначаем день и<br/>время встречи</div>
                        </li>
                        <li>
                            <div class="stage__icon">
                                <i>
                                    <img src="img/stage__03.svg" class="img-fluid" alt="">
                                </i>
                            </div>
                            <div class="stage__text">Проводим<br/>консультацию</div>
                        </li>
                        <li>
                            <div class="stage__icon">
                                <i>
                                    <img src="img/stage__04.svg" class="img-fluid" alt="">
                                </i>
                            </div>
                            <div class="stage__text">Получаете детальный план внедрения алгоритмов для Вашего бизнеса</div>
                        </li>
                    </ul>

                    <div class="heading mb_50">
                        <h2>Среди наших успешных клиентов</h2>
                    </div>
                    <ul class="clients mb_40">
                        <li><img src="images/logo__01.png" alt=""></li>
                        <li><img src="images/logo__02.png" alt=""></li>
                        <li><img src="images/logo__03.png" alt=""></li>
                        <li><img src="images/logo__04.png" alt=""></li>
                        <li><img src="images/logo__05.png" alt=""></li>
                        <li><img src="images/logo__06.png" alt=""></li>
                        <li><img src="images/logo__07.png" alt=""></li>
                    </ul>


                    <div class="heading">
                        <h2>...присоединяйтесь, будьте среди лучших! <a class="phone_number" href="tel:+7499)9990132">+ 7(499)999-01-32</a></h2>
                    </div>


                </div>
            </section>

            <footer class="footer">
                <div class="container">
                    <div class="footer__row">
                        <div class="footer__row_left">
                            <div class="footer__logo">
                                <img src="img/footer__logo.svg" class="img-fluid" alt="">
                            </div>
                            <div class="footer__text">
                                ООО «Авиант Прайм»<br/>
                                Е-mail: info@xxxxxx.ru<br/>
                                ИНН

                            </div>
                        </div>
                        <div class="footer__row_right">
                            <ul class="footer__contact">
                                <li>
                                    <a class="footer__phone" href="tel:+74999990132">+ 7(499)999-01-32</a>
                                </li>
                                <li>
                                    <a href="#" class="btn text-uppercase">Бесплатный звонок</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>

            <div class="bottom_line">
                <div class="container">
                    <a href="#">Политика конфиденциальности</a>
                </div>
            </div>

            <div class="map" id="map">

            </div>

        </div>


        <script src="js/vendor/modernizr-3.5.0.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>
        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
