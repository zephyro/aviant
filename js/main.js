
$(".btn_modal").fancybox({
    'padding'    : 0
});


var result = new Swiper('.result__slider', {
    loop: true,
    navigation: {
        nextEl: '.slider_button_next',
        prevEl: '.slider_button_prev',
    },
    pagination: {
        clickable: true,
        el: '.swiper-pagination',
    }
});


// Expert tab

(function() {

    $('.expert__nav a').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("href"));
        var box = $('.expert__user');

        $(this).closest('.expert__nav').find('a').removeClass('active');
        $(this).addClass('active');

        box.find('.expert__user_item').removeClass('active');
        box.find(tab).addClass('active');
    });

}());


ymaps.ready(init);

function init () {
    var myMap = new ymaps.Map("map", {
        center: [55.716021569023546,37.646479999999926],
        zoom: 10,
        controls: ['smallMapDefaultSet']
    });

    myMap.geoObjects
        .add(new ymaps.Placemark([55.716021569023546,37.646479999999926], {
            balloonContent: 'Москва, Павелецкая набережная, д.2'
        }, {
            preset: 'islands#icon',
            iconColor: '#0095b6'
        }));
}